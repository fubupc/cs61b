public class Pixel {
    public short red = 0;
    public short green = 0;
    public short blue = 0;

    public Pixel() {
    }

    public Pixel(short red, short green, short blue) {
        this.red = red;
        this.green = green;
        this.blue = blue;
    }

    public Pixel(int red, int green, int blue) {
        this.red = (short)red;
        this.green = (short)green;
        this.blue = (short)blue;
    }

    public String toString() {
        return "(" + red + ", " + green + ", " + blue + ")";
    }

    public static boolean validIntensity(short intensity) {
        return (intensity >= 0) && (intensity <= 255);
    }

}
