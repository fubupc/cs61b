class Base {
    public static int count = 0;

    public String name;

    public Base() {
        count++;
        name = "Base-" + count;
    }

    public String conflict(int i) {
       return "Base.conflict(" + i + ")"; 
    }

    public String doSome() {
       return "Base.doSome()"; 
    }

    public static final String some = "Base";
    public String other = "Base";
}
