class Sub extends Base implements Face {
    public Sub() {
        name = "Sub-" + count;
    }

    // override Sub.doSome()
    public String doSome() {
       return "Sub.doSome()"; 
    }

    public String other = "Sub";
}
