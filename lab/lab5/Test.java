class Test {
    public static void main(String[] args) {

        // I.
        //Base[] xa = {new Sub(), new Sub()};  // xa real type is Base[];
        //Sub[] ya = new Sub[6];
        //assert xa instanceof Sub[];
        //ya = (Sub[]) xa; // compile ok, runtime error.

        //Base[] xa = new Sub[3]; // xa's real type is Sub[];
        //Sub[] ya = new Sub[6];
        //assert xa instanceof Sub[];
        //ya = (Sub[]) xa; // compile ok, runtime ok.


        // II.
        // a. compile ok.
        // b. compile-error.
        // c. compile-error.
        // d. compile-ok.
        // Note: Everytime you change Face.java you should ***DELETE*** all
        // *.class files and then re-compile!!! (maybe a javac bug)
        Base x = new Base();
        Sub y = new Sub();
        Base z = new Sub();

        System.out.println(x.name);
        System.out.println(x.conflict(3));

        System.out.println(y.name);
        System.out.println(y.conflict(5));

        System.out.println(z.name);
        System.out.println(z.conflict(8));

        // III.
        Base m = new Sub();
        Face n = new Sub();
        Sub i = new Sub();

        System.out.println("(Base)m.some:" + m.some);
        System.out.println("(Base)m.other:" + m.other); // polymorphism doesn't apply to fields
        System.out.println("(Base)m.conflict:" + m.conflict(5));

        System.out.println("(Face)n.some:" + n.some);
        //System.out.println("(Face)n.other:" + n.other); // compile-error.

        //System.out.println("(Sub)i.some:" + i.some); //compile-error
        System.out.println("(Sub)i.other:" + i.other);
        System.out.println("(Sub)i.conflict:" + i.conflict(5));


        // IV.
        // a. call subclass method
        Sub a = new Sub();
        System.out.println(((Base) a).doSome());

        // b. error.
        Base b = new Base();
        //System.out.println(((Sub) b).doSome()); //compile-error. SubClass cannot cast to SuperClass.
        //
        // c. cannot have a subclass object but call superclass method on it. (not really sure!!!)
    }
}
