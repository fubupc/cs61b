/* BadTransactionException.java */

/**
 **/
public class BadTransactionException extends Exception {

  /**
   **/
  public BadTransactionException(String message) {
      super(message);
  }

}
