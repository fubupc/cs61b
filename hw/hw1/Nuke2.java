/* Nuke2.java */

import java.io.*;

class Nuke2 {

  public static void main(String[] arg) throws Exception {

    BufferedReader keyboard;
    String inputLine;

    keyboard = new BufferedReader(new InputStreamReader(System.in));

    inputLine = keyboard.readLine();

    /* method 1 */
    for (int i = 0; i < inputLine.length(); i++) {
        if (i != 1)
            System.out.print(inputLine.charAt(i));
    }

  }
}
