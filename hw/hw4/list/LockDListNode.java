package list;

public class LockDListNode extends DListNode {

    protected boolean locked;

    public LockDListNode(Object i, DListNode p, DListNode n) {
        super(i, (LockDListNode) p, (LockDListNode) n);
        locked = false;
    }

    public boolean isLocked() {
        return locked;
    }

    public void lock() {
        locked = true;
    }

}

