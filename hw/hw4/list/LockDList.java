package list;

public class LockDList extends DList {

    public void lockNode(DListNode node) {
        ((LockDListNode) node).lock();
    }

    protected DListNode newNode(Object item, DListNode prev, DListNode next) {
        return new LockDListNode(item, (LockDListNode) prev, (LockDListNode) next);
    }

    public void remove(DListNode node) {
        LockDListNode _node = (LockDListNode) node;

        if (null != _node && !_node.isLocked()) {
            super.remove(_node);
        }
    }

}
