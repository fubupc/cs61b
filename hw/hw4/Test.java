import list.*;

class Test {
    public static void main(String[] args) {
        DList d = new DList();
        System.out.println("d should be emtpy:" + d);

        d.insertFront("1");
        System.out.println("d should be [1]:" + d);

        d.insertBack("2");
        System.out.println("d should be [1, 2]:" + d);

        d.remove(d.prev(d.front()));
        // note: infinite loop.
        // System.out.println("d is corrupted (sentinel deleted):" + d);

        LockDList l = new LockDList();
        System.out.println("l should be emtpy:" + l);

        l.insertFront("1");
        System.out.println("l should be [1]:" + l);

        l.lockNode(l.front());
        l.remove(l.front());
        System.out.println("l first node is locked. l should be still [1]:" + l);

        l.insertBack("2");
        System.out.println("l should be [1, 2]:" + l);

        l.remove(l.back());
        System.out.println("l should be [1]:" + l);

    }

}
