/* Set.java */

import list.*;

/**
 *  A Set is a collection of Comparable elements stored in sorted order.
 *  Duplicate elements are not permitted in a Set.
 **/
public class Set {
  /* Fill in the data fields here. */
    List list;

  /**
   * Set ADT invariants:
   *  1)  The Set's elements must be precisely the elements of the List.
   *  2)  The List must always contain Comparable elements, and those elements 
   *      must always be sorted in ascending order.
   *  3)  No two elements in the List may be equal according to compareTo().
   **/

  /**
   *  Constructs an empty Set. 
   *
   *  Performance:  runs in O(1) time.
   **/
  public Set() { 
    // Your solution here.
    list = new DList();
  }

  /**
   *  cardinality() returns the number of elements in this Set.
   *
   *  Performance:  runs in O(1) time.
   **/
  public int cardinality() {
    // Replace the following line with your solution.
    return list.length();
  }

  /**
   *  insert() inserts a Comparable element into this Set.
   *
   *  Sets are maintained in sorted order.  The ordering is specified by the
   *  compareTo() method of the java.lang.Comparable interface.
   *
   *  Performance:  runs in O(this.cardinality()) time.
   **/
  public void insert(Comparable c) {
      // note: code will be more clean if we use ListNode.insertBefore(),
      // but SListNode.insertBefore() has O(n) time complexity and 
      // SListNode.insertAfter() has only O(1) complexity.

      ListNode prev = null;

      if (list.isEmpty()) { 
          list.insertFront(c);
          return;
      } 

      try {
          ListNode current = list.front();
          while (current.isValidNode()) {
              int result = c.compareTo(current.item());
              if (result == 0) {
                  return;
              } else if (result < 0) {
                  if (prev == null) {
                      list.insertFront(c);
                  } else {
                      prev.insertAfter(c);
                  }
                  return;
              }
              prev = current;
              current = current.next();
          }

          // "this" set has came to an end. so just append the item to the last.
          list.insertBack(c);
      } catch (InvalidNodeException e) {
          //
      }
  }

  /**
   *  union() modifies this Set so that it contains all the elements it
   *  started with, plus all the elements of s.  The Set s is NOT modified.
   *  Make sure that duplicate elements are not created.
   *
   *  Performance:  Must run in O(this.cardinality() + s.cardinality()) time.
   *
   *  Your implementation should NOT copy elements of s or "this", though it
   *  will copy _references_ to the elements of s.  Your implementation will
   *  create new _nodes_ for the elements of s that are added to "this", but
   *  you should reuse the nodes that are already part of "this".
   *
   *  DO NOT MODIFY THE SET s.
   *  DO NOT ATTEMPT TO COPY ELEMENTS; just copy _references_ to them.
   **/
  public void union(Set s) {
    // Your solution here.

    ListNode mePrev = null;
    ListNode oppo = s.list.front();

    try {
        ListNode meCurrent = list.front();
        while (oppo.isValidNode()) {
            if (meCurrent.isValidNode()) {
                Comparable item = (Comparable) oppo.item();
                int result = item.compareTo(meCurrent.item());
                if (result == 0) {
                    oppo = oppo.next();
                } else if (result < 0) {
                    if (mePrev == null) {
                        list.insertFront(item);
                    } else {
                        mePrev.insertAfter(item);
                    }
                    oppo = oppo.next();
                } else {
                    mePrev = meCurrent;
                    meCurrent = meCurrent.next();
                }
            } else {
                list.insertBack(oppo.item());
                oppo = oppo.next();
            }
        }
    } catch (InvalidNodeException e) {
        // actually impossible.
    }
  }

  /**
   *  intersect() modifies this Set so that it contains the intersection of
   *  its own elements and the elements of s.  The Set s is NOT modified.
   *
   *  Performance:  Must run in O(this.cardinality() + s.cardinality()) time.
   *
   *  Do not construct any new ListNodes during the execution of intersect.
   *  Reuse the nodes of "this" that will be in the intersection.
   *
   *  DO NOT MODIFY THE SET s.
   *  DO NOT CONSTRUCT ANY NEW NODES.
   *  DO NOT ATTEMPT TO COPY ELEMENTS.
   **/
  public void intersect(Set s) {
    // Your solution here.
    ListNode me = list.front();
    ListNode oppo = s.list.front();

    try {
        while (me.isValidNode()) {
            if (oppo.isValidNode()) {
                int result = ((Comparable) oppo.item()).compareTo(me.item());
                if (result == 0) {
                    me = me.next();
                    oppo = oppo.next();
                } else if (result < 0) {
                    oppo = oppo.next();
                } else {
                    ListNode tmp = me;
                    me = me.next();
                    tmp.remove();
                }
            } else {
                ListNode tmp = me;
                me = me.next();
                tmp.remove();
            }
        }
    } catch (InvalidNodeException e) {
        // impossible
    }
  }

  /**
   *  toString() returns a String representation of this Set.  The String must
   *  have the following format:
   *    {  } for an empty Set.  No spaces before "{" or after "}"; two spaces
   *            between them.
   *    {  1  2  3  } for a Set of three Integer elements.  No spaces before
   *            "{" or after "}"; two spaces before and after each element.
   *            Elements are printed with their own toString method, whatever
   *            that may be.  The elements must appear in sorted order, from
   *            lowest to highest according to the compareTo() method.
   *
   *  WARNING:  THE AUTOGRADER EXPECTS YOU TO PRINT SETS IN _EXACTLY_ THIS
   *            FORMAT, RIGHT UP TO THE TWO SPACES BETWEEN ELEMENTS.  ANY
   *            DEVIATIONS WILL LOSE POINTS.
   **/
  public String toString() {
    // Replace the following line with your solution.
    String out = "{  ";

    try {
        ListNode current = list.front();

        while (current.isValidNode()) {
            out += current.item() + "  ";
            current = current.next();
        }
    } catch (InvalidNodeException e) {
        // actually this exception cannot happen.
    }

    return out + "}";
  }

  public static void main(String[] argv) {
    Set s = new Set();
    s.insert(new Integer(3));
    s.insert(new Integer(4));
    s.insert(new Integer(3));
    System.out.println("Set s = " + s);

    Set s2 = new Set();
    s2.insert(new Integer(4));
    s2.insert(new Integer(5));
    s2.insert(new Integer(5));
    System.out.println("Set s2 = " + s2);

    Set s3 = new Set();
    s3.insert(new Integer(5));
    s3.insert(new Integer(3));
    s3.insert(new Integer(8));
    System.out.println("Set s3 = " + s3);

    s.union(s2);
    System.out.println("After s.union(s2), s = " + s);

    s.intersect(s3);
    System.out.println("After s.intersect(s3), s = " + s);

    System.out.println("s.cardinality() = " + s.cardinality());
    // You may want to add more (ungraded) test code here.
  }
}
