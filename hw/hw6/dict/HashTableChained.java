/* HashTableChained.java */

package dict;
import list.*;

/**
 *  HashTableChained implements a Dictionary as a hash table with chaining.
 *  All objects used as keys must have a valid hashCode() method, which is
 *  used to determine which bucket of the hash table an entry is stored in.
 *  Each object's hashCode() is presumed to return an int between
 *  Integer.MIN_VALUE and Integer.MAX_VALUE.  The HashTableChained class
 *  implements only the compression function, which maps the hash code to
 *  a bucket in the table's range.
 *
 *  DO NOT CHANGE ANY PROTOTYPES IN THIS FILE.
 **/

public class HashTableChained implements Dictionary {

  /**
   *  Place any data fields here.
   **/

    private List[] buckets;
    private int size;
    private int collisionTotal = 0;
    private int compCollisionNum = 0;
    private int hashCollisionNum = 0;

    private static final int DEFAULT_SIZE = 101;

  /** 
   *  Construct a new empty hash table intended to hold roughly sizeEstimate
   *  entries.  (The precise number of buckets is up to you, but we recommend
   *  you use a prime number, and shoot for a load factor between 0.5 and 1.)
   **/

  public HashTableChained(int sizeEstimate) {
    // Your solution here.
    buckets = new SList[calcBucketLength(sizeEstimate)];
    //buckets = new SList[sizeEstimate];
    size = 0;
  }

  public int calcBucketLength(int sizeEstimate) {
      int i = sizeEstimate * 4 / 3;
      while (true) {
          if (isPrime(i)) {
              return i;
          }
          i++;
      }
  }

  public boolean isPrime(int n) {
      int root = (int) Math.sqrt(n);
      for (int i = 2; i <= root; i++) {
          if (n % i == 0) {
              return false;
          }
      }
      return true;
  }

  /** 
   *  Construct a new empty hash table with a default size.  Say, a prime in
   *  the neighborhood of 100.
   **/

  public HashTableChained() {
    // Your solution here.
    this(DEFAULT_SIZE);
  }

  /**
   *  Converts a hash code in the range Integer.MIN_VALUE...Integer.MAX_VALUE
   *  to a value in the range 0...(size of hash table) - 1.
   *
   *  This function should have package protection (so we can test it), and
   *  should be used by insert, find, and remove.
   **/

  int compFunction(int code) {
    // Replace the following line with your solution.
        return Math.abs(code) % buckets.length; //buckets.length;
//    return ((Math.abs(code) * 67 + 137) % 16908799) % buckets.length;
  }

  /** 
   *  Returns the number of entries stored in the dictionary.  Entries with
   *  the same key (or even the same key and value) each still count as
   *  a separate entry.
   *  @return number of entries in the dictionary.
   **/

  public int size() {
    // Replace the following line with your solution.
    return size;
  }


  public int bucketSize() {
      return buckets.length;
  }

  public int collisionTotal() {
      return collisionTotal;
  }

  public int compCollisionNum() {
      return compCollisionNum;
  }

  public int hashCollisionNum() {
      return hashCollisionNum;
  }

  /** 
   *  Tests if the dictionary is empty.
   *
   *  @return true if the dictionary has no entries; false otherwise.
   **/

  public boolean isEmpty() {
    // Replace the following line with your solution.
    return size == 0;
  }

  /**
   *  Create a new Entry object referencing the input key and associated value,
   *  and insert the entry into the dictionary.  Return a reference to the new
   *  entry.  Multiple entries with the same key (or even the same key and
   *  value) can coexist in the dictionary.
   *
   *  This method should run in O(1) time if the number of collisions is small.
   *
   *  @param key the key by which the entry can be retrieved.
   *  @param value an arbitrary object.
   *  @return an entry containing the key and value.
   **/

  public Entry insert(Object key, Object value) {
    // Replace the following line with your solution.
    Entry e = new Entry();
    e.key = key;
    e.value = value;

    int index = compFunction(key.hashCode());

    if (buckets[index] == null) {
        buckets[index] = new SList();
    }

    if (buckets[index].length() > 0) {
        collisionTotal++;

        // real collision means: different hash code fall into same bucket.
        try {
            boolean collision = true;
            for (ListNode n = buckets[index].front(); n.isValidNode(); n = n.next()) {
                int newCode = key.hashCode();
                int oldCode = ((Entry) n.item()).key.hashCode();
                if (newCode == oldCode) {
                    hashCollisionNum++;
                    collision = false;
                    break;
                }
            }
            if (collision) {
                compCollisionNum++;
            }
        } catch (InvalidNodeException i) {
            //
        }
    }

    buckets[index].insertBack(e);
    // System.out.println("buckets[" + index + "] length: " + buckets[index].length());
    size++;

    return e;
  }

  /** 
   *  Search for an entry with the specified key.  If such an entry is found,
   *  return it; otherwise return null.  If several entries have the specified
   *  key, choose one arbitrarily and return it.
   *
   *  This method should run in O(1) time if the number of collisions is small.
   *
   *  @param key the search key.
   *  @return an entry containing the key and an associated value, or null if
   *          no entry contains the specified key.
   **/

  public Entry find(Object key) {
    // Replace the following line with your solution.
    int index = compFunction(key.hashCode());
    if (buckets[index] != null) {
        try {
            ListNode current = buckets[index].front();
            while (current.isValidNode()) {
                Entry e = (Entry) current.item();
                if (key.equals(e.key)) {
                    return e;
                }
                current = current.next();
            }
        } catch(InvalidNodeException e) {
            // impossible
        }
    }

    return null;
  }

  /** 
   *  Remove an entry with the specified key.  If such an entry is found,
   *  remove it from the table and return it; otherwise return null.
   *  If several entries have the specified key, choose one arbitrarily, then
   *  remove and return it.
   *
   *  This method should run in O(1) time if the number of collisions is small.
   *
   *  @param key the search key.
   *  @return an entry containing the key and an associated value, or null if
   *          no entry contains the specified key.
   */

  public Entry remove(Object key) {
    // Replace the following line with your solution.
    int index = compFunction(key.hashCode());
    List list = buckets[index];

    if (list != null) {
        try {
            for (ListNode n = list.front(); n.isValidNode(); n = n.next()) {
                Entry e = (Entry) n.item();
                if (key.equals(e.key)) {
                    n.remove();
                    size--;
                    return e;
                }
            }
        } catch (InvalidNodeException e) {
            // impossible
        }
    }

    return null;
  }

  /**
   *  Remove all entries from the dictionary.
   */
  public void makeEmpty() {
    // Your solution here.
    buckets = new SList[buckets.length];
  }

}
